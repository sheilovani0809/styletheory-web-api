const respMesCode = require('./../helper/response_message.json');

const scenario = ({
    // happy case
    getSuccess: {
        successLogin: 'User can login by email account and password',
        successLoginFB: 'User can login by Facebook account and password',
        successLoginGoogle: 'User can login by Google account and password',
        response: respMesCode.successOk,
    },
    getFailed: {
      failedLoginIfInvalidToken: {
        Desc: 'get error when token invalid',
        response: respMesCode.failedForbiddenInvalidToken.codeNumber,
      },
      failedLoginWithoutCredential: {
        Desc: 'get error when token blank',
        response: respMesCode.failedUnauthorized.codeNumber,
      },
    },
  });
  
  module.exports = {
    scenario,
  };