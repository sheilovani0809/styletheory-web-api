const supertest = require('supertest');
const env = require('dotenv').config();

const api = supertest(process.env.API_BASE_URL);

const postLogin = body => api
    .post('/access_token')
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .send(body);

module.exports = {
    postLogin,
}