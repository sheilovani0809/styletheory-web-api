/* eslint-env mocha */
/* eslint prefer-destructuring: off */
/* eslint no-unused-expressions: off */

const { expect } = require('chai');
const chai = require('chai');
const mocha = require('mocha');

const responseMessageCode = require('./../helper/response_message.json');
const userCredential = require('./../helper/data_user.json');
const testcase = require('./../testcase/tc_login.js');
const page = require('./../page/page_login.js');


describe(`@loginStyleTheory @post`, () => {
    it(`${testcase.scenario.getSuccess.successLogin}`, async () => {
        const response = await page.postLogin(userCredential);
        expect(response.status).to.equal(responseMessageCode.successOk);
    });
});


describe(`@logoutStyleTheory @post`, () => {
    it(`${testcase.scenario.getSuccess.successLogin}`, async () => {
        const response = await page.postLogin(userCredential);
        expect(response.status).to.equal(responseMessageCode.successOk);
    });
});